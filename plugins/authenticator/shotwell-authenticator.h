/* shotwell-authenticator.h generated by valac 0.40.4, the Vala compiler, do not modify */


#ifndef __PLUGINS_AUTHENTICATOR_SHOTWELL_AUTHENTICATOR_H__
#define __PLUGINS_AUTHENTICATOR_SHOTWELL_AUTHENTICATOR_H__

#include <glib.h>
#include <glib-object.h>
#include "shotwell-plugin-dev-1.0.h"

G_BEGIN_DECLS


#define PUBLISHING_AUTHENTICATOR_TYPE_FACTORY (publishing_authenticator_factory_get_type ())
#define PUBLISHING_AUTHENTICATOR_FACTORY(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), PUBLISHING_AUTHENTICATOR_TYPE_FACTORY, PublishingAuthenticatorFactory))
#define PUBLISHING_AUTHENTICATOR_FACTORY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), PUBLISHING_AUTHENTICATOR_TYPE_FACTORY, PublishingAuthenticatorFactoryClass))
#define PUBLISHING_AUTHENTICATOR_IS_FACTORY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PUBLISHING_AUTHENTICATOR_TYPE_FACTORY))
#define PUBLISHING_AUTHENTICATOR_IS_FACTORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), PUBLISHING_AUTHENTICATOR_TYPE_FACTORY))
#define PUBLISHING_AUTHENTICATOR_FACTORY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), PUBLISHING_AUTHENTICATOR_TYPE_FACTORY, PublishingAuthenticatorFactoryClass))

typedef struct _PublishingAuthenticatorFactory PublishingAuthenticatorFactory;
typedef struct _PublishingAuthenticatorFactoryClass PublishingAuthenticatorFactoryClass;
typedef struct _PublishingAuthenticatorFactoryPrivate PublishingAuthenticatorFactoryPrivate;

struct _PublishingAuthenticatorFactory {
	GObject parent_instance;
	PublishingAuthenticatorFactoryPrivate * priv;
};

struct _PublishingAuthenticatorFactoryClass {
	GObjectClass parent_class;
};


GType publishing_authenticator_factory_get_type (void) G_GNUC_CONST;
PublishingAuthenticatorFactory* publishing_authenticator_factory_get_instance (void);
PublishingAuthenticatorFactory* publishing_authenticator_factory_new (void);
PublishingAuthenticatorFactory* publishing_authenticator_factory_construct (GType object_type);


G_END_DECLS

#endif
