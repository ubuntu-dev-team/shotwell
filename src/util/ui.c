/* ui.c generated by valac 0.40.4, the Vala compiler
 * generated from ui.vala, do not modify */

/* Copyright 2016 Software Freedom Conservancy Inc.
 *
 * This software is licensed under the GNU Lesser General Public License
 * (version 2.1 or later).  See the COPYING file in this distribution.
 */


#include <glib.h>
#include <glib-object.h>
#include "shotwell-plugin-dev-1.0.h"
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>


#define TYPE_ADJUSTMENT_RELATION (adjustment_relation_get_type ())

#define TYPE_COMPASS_POINT (compass_point_get_type ())

#define TYPE_DIRECTION (direction_get_type ())

typedef enum  {
	ADJUSTMENT_RELATION_BELOW,
	ADJUSTMENT_RELATION_IN_RANGE,
	ADJUSTMENT_RELATION_ABOVE
} AdjustmentRelation;

typedef enum  {
	COMPASS_POINT_NORTH,
	COMPASS_POINT_SOUTH,
	COMPASS_POINT_EAST,
	COMPASS_POINT_WEST
} CompassPoint;

typedef enum  {
	DIRECTION_FORWARD,
	DIRECTION_BACKWARD
} Direction;



GType adjustment_relation_get_type (void) G_GNUC_CONST;
GType compass_point_get_type (void) G_GNUC_CONST;
GType direction_get_type (void) G_GNUC_CONST;
SpitTransitionsDirection direction_to_transition_direction (Direction self);
const gchar* direction_to_string (Direction self);
void spin_event_loop (void);
AdjustmentRelation get_adjustment_relation (GtkAdjustment* adjustment,
                                            gint value);
void get_adjustment_page (GtkAdjustment* hadj,
                          GtkAdjustment* vadj,
                          GdkRectangle* result);
gboolean has_only_key_modifier (GdkModifierType field,
                                GdkModifierType mask);


GType
adjustment_relation_get_type (void)
{
	static volatile gsize adjustment_relation_type_id__volatile = 0;
	if (g_once_init_enter (&adjustment_relation_type_id__volatile)) {
		static const GEnumValue values[] = {{ADJUSTMENT_RELATION_BELOW, "ADJUSTMENT_RELATION_BELOW", "below"}, {ADJUSTMENT_RELATION_IN_RANGE, "ADJUSTMENT_RELATION_IN_RANGE", "in-range"}, {ADJUSTMENT_RELATION_ABOVE, "ADJUSTMENT_RELATION_ABOVE", "above"}, {0, NULL, NULL}};
		GType adjustment_relation_type_id;
		adjustment_relation_type_id = g_enum_register_static ("AdjustmentRelation", values);
		g_once_init_leave (&adjustment_relation_type_id__volatile, adjustment_relation_type_id);
	}
	return adjustment_relation_type_id__volatile;
}


GType
compass_point_get_type (void)
{
	static volatile gsize compass_point_type_id__volatile = 0;
	if (g_once_init_enter (&compass_point_type_id__volatile)) {
		static const GEnumValue values[] = {{COMPASS_POINT_NORTH, "COMPASS_POINT_NORTH", "north"}, {COMPASS_POINT_SOUTH, "COMPASS_POINT_SOUTH", "south"}, {COMPASS_POINT_EAST, "COMPASS_POINT_EAST", "east"}, {COMPASS_POINT_WEST, "COMPASS_POINT_WEST", "west"}, {0, NULL, NULL}};
		GType compass_point_type_id;
		compass_point_type_id = g_enum_register_static ("CompassPoint", values);
		g_once_init_leave (&compass_point_type_id__volatile, compass_point_type_id);
	}
	return compass_point_type_id__volatile;
}


SpitTransitionsDirection
direction_to_transition_direction (Direction self)
{
	SpitTransitionsDirection result = 0;
#line 25 "/home/jens/Source/shotwell/src/util/ui.vala"
	switch (self) {
#line 25 "/home/jens/Source/shotwell/src/util/ui.vala"
		case DIRECTION_FORWARD:
#line 98 "ui.c"
		{
#line 27 "/home/jens/Source/shotwell/src/util/ui.vala"
			result = SPIT_TRANSITIONS_DIRECTION_FORWARD;
#line 27 "/home/jens/Source/shotwell/src/util/ui.vala"
			return result;
#line 104 "ui.c"
		}
#line 25 "/home/jens/Source/shotwell/src/util/ui.vala"
		case DIRECTION_BACKWARD:
#line 108 "ui.c"
		{
#line 30 "/home/jens/Source/shotwell/src/util/ui.vala"
			result = SPIT_TRANSITIONS_DIRECTION_BACKWARD;
#line 30 "/home/jens/Source/shotwell/src/util/ui.vala"
			return result;
#line 114 "ui.c"
		}
		default:
		{
			GEnumValue* _tmp0_;
#line 33 "/home/jens/Source/shotwell/src/util/ui.vala"
			_tmp0_ = g_enum_get_value (g_type_class_ref (TYPE_DIRECTION), self);
#line 33 "/home/jens/Source/shotwell/src/util/ui.vala"
			g_error ("ui.vala:33: Unknown Direction %s", (_tmp0_ != NULL) ? _tmp0_->value_name : NULL);
#line 123 "ui.c"
		}
	}
}


GType
direction_get_type (void)
{
	static volatile gsize direction_type_id__volatile = 0;
	if (g_once_init_enter (&direction_type_id__volatile)) {
		static const GEnumValue values[] = {{DIRECTION_FORWARD, "DIRECTION_FORWARD", "forward"}, {DIRECTION_BACKWARD, "DIRECTION_BACKWARD", "backward"}, {0, NULL, NULL}};
		GType direction_type_id;
		direction_type_id = g_enum_register_static ("Direction", values);
		g_once_init_leave (&direction_type_id__volatile, direction_type_id);
	}
	return direction_type_id__volatile;
}


void
spin_event_loop (void)
{
#line 39 "/home/jens/Source/shotwell/src/util/ui.vala"
	while (TRUE) {
#line 39 "/home/jens/Source/shotwell/src/util/ui.vala"
		if (!gtk_events_pending ()) {
#line 39 "/home/jens/Source/shotwell/src/util/ui.vala"
			break;
#line 152 "ui.c"
		}
#line 40 "/home/jens/Source/shotwell/src/util/ui.vala"
		gtk_main_iteration ();
#line 156 "ui.c"
	}
}


AdjustmentRelation
get_adjustment_relation (GtkAdjustment* adjustment,
                         gint value)
{
	AdjustmentRelation result = 0;
#line 43 "/home/jens/Source/shotwell/src/util/ui.vala"
	g_return_val_if_fail (GTK_IS_ADJUSTMENT (adjustment), 0);
#line 44 "/home/jens/Source/shotwell/src/util/ui.vala"
	if (value < ((gint) gtk_adjustment_get_value (adjustment))) {
#line 45 "/home/jens/Source/shotwell/src/util/ui.vala"
		result = ADJUSTMENT_RELATION_BELOW;
#line 45 "/home/jens/Source/shotwell/src/util/ui.vala"
		return result;
#line 174 "ui.c"
	} else {
#line 46 "/home/jens/Source/shotwell/src/util/ui.vala"
		if (value > ((gint) (gtk_adjustment_get_value (adjustment) + gtk_adjustment_get_page_size (adjustment)))) {
#line 47 "/home/jens/Source/shotwell/src/util/ui.vala"
			result = ADJUSTMENT_RELATION_ABOVE;
#line 47 "/home/jens/Source/shotwell/src/util/ui.vala"
			return result;
#line 182 "ui.c"
		} else {
#line 49 "/home/jens/Source/shotwell/src/util/ui.vala"
			result = ADJUSTMENT_RELATION_IN_RANGE;
#line 49 "/home/jens/Source/shotwell/src/util/ui.vala"
			return result;
#line 188 "ui.c"
		}
	}
}


void
get_adjustment_page (GtkAdjustment* hadj,
                     GtkAdjustment* vadj,
                     GdkRectangle* result)
{
	GdkRectangle rect = {0};
#line 52 "/home/jens/Source/shotwell/src/util/ui.vala"
	g_return_if_fail (GTK_IS_ADJUSTMENT (hadj));
#line 52 "/home/jens/Source/shotwell/src/util/ui.vala"
	g_return_if_fail (GTK_IS_ADJUSTMENT (vadj));
#line 53 "/home/jens/Source/shotwell/src/util/ui.vala"
	memset (&rect, 0, sizeof (GdkRectangle));
#line 54 "/home/jens/Source/shotwell/src/util/ui.vala"
	rect.x = (gint) gtk_adjustment_get_value (hadj);
#line 55 "/home/jens/Source/shotwell/src/util/ui.vala"
	rect.y = (gint) gtk_adjustment_get_value (vadj);
#line 56 "/home/jens/Source/shotwell/src/util/ui.vala"
	rect.width = (gint) gtk_adjustment_get_page_size (hadj);
#line 57 "/home/jens/Source/shotwell/src/util/ui.vala"
	rect.height = (gint) gtk_adjustment_get_page_size (vadj);
#line 59 "/home/jens/Source/shotwell/src/util/ui.vala"
	*result = rect;
#line 59 "/home/jens/Source/shotwell/src/util/ui.vala"
	return;
#line 218 "ui.c"
}


gboolean
has_only_key_modifier (GdkModifierType field,
                       GdkModifierType mask)
{
	gboolean result = FALSE;
#line 77 "/home/jens/Source/shotwell/src/util/ui.vala"
	result = (field & ((((((((GDK_SHIFT_MASK | GDK_CONTROL_MASK) | GDK_MOD1_MASK) | GDK_MOD3_MASK) | GDK_MOD4_MASK) | GDK_MOD5_MASK) | GDK_SUPER_MASK) | GDK_HYPER_MASK) | GDK_META_MASK)) == mask;
#line 77 "/home/jens/Source/shotwell/src/util/ui.vala"
	return result;
#line 231 "ui.c"
}



