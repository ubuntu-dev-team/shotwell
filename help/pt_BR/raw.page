<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="other-raw" xml:lang="pt-BR">

    <info>
        <link type="guide" xref="index#other"/>
        <desc>Mais informações sobre o suporte de arquivos RAW no Shotwell.</desc>
        
        <link type="next" xref="running"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felipe Braga</mal:name>
      <mal:email>fbobraga@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>
   
    <title>Suporte de arquivos RAW no Shotwell</title> 
    <p>Algumas câmeras têm a capacidade de armazenar em um arquivo dados diretamente do sensor e que contém informações adicionais de cor; este formato de arquivo é comumente chamado de 'RAW' ou 'câmera RAW', e o Shotwell também oferece suporte a esse tipo de arquivo.</p>
    
    <p>Como fotos RAW normalmente não podem ser exibidas diretamente, mas precisam ser tratadas antes - precisam da sua informação adicional corretamente interpretada para possibilitar a exibição - a maioria das câmeras incorpora uma imagem JPEG dentro de um arquivo RAW, ou gera uma imagem JPEG junto com o arquivo RAW no momento em que foto é tirada. Este segundo formato é chamado ao longo deste documento como "RAW+JPEG". Se você importar um par de arquivos RAW+JPEG, o Shotwell irá mantê-los emparelhados e tratá-los como um único item na sua biblioteca.</p>
    
    <p>Quando você importa um arquivo RAW, você pode optar por usar a imagem JPEG gerada pela câmera ou uma gerada pelo Shotwell no menu em <guiseq><gui>Fotos</gui><gui>Desenvolvedor</gui></guiseq>.</p>

    <note>
        <p>Mudar o desenvolvedor de uma foto fará com que todas as alterações feitas nela pelo Shotwell sejam descartadas.</p>
    </note>
    
    <p>Para publicar ou utilizar uma foto RAW na maioria dos outros softwares, você precisará antes exportá-la. O Shotwell pode exportar suas fotos RAW nos formatos JPEG, PNG, TIFF ou BMP, e, se uma foto RAW for publicada, ela será internamente exportada para JPEG e é imagem exportada que será publicada.</p>  
    
</page>
