<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="tag" xml:lang="pt-BR">

    <info>
        <link type="guide" xref="index#organize"/>
        <desc>Organize suas fotos com rótulos.</desc>
        
        <link type="seealso" xref="event"/>
        
        <link type="next" xref="title"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felipe Braga</mal:name>
      <mal:email>fbobraga@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

	<title>Etiquetando fotos</title>

    <p>Você pode atribuir uma ou mais etiquetas para as fotos selecionadas. Uma etiqueta é composta de uma ou mais palavras que serão associadas às fotos.</p>
    
    <p>Para adicionar novas etiquetas nas fotos, selecione as fotos que você gostaria de etiquetar, e:</p>
    
    <list>
		<item><p>Use o item de menu <guiseq><gui>Etiquetas</gui><gui>Adicionar etiquetas...</gui></guiseq>.</p></item>
		<item><p>Pressione as teclas <keyseq><key>Ctrl</key><key>T</key></keyseq>.</p></item>
		<item><p>Arraste as fotos selecionadas até etiqueta desejada, na barra lateral.</p></item>
    </list>
     
    <p>Quando você usa o <keyseq><key>Ctrl</key><key>T</key></keyseq> ou <guiseq><gui>Etiquetas</gui><gui>Adicionar etiquetas...</gui></guiseq> você poderá digitar os nomes de uma ou mais etiquetas, separados por vírgulas. Depois de ter criado uma etiqueta, você pode renomeá-la selecionando essa etiqueta na barra lateral e escolher <guiseq><gui>Etiquetas</gui><gui>Renomear etiqueta "[nome]"...</gui></guiseq>, ou clicar com o botão direito nela e usar o item <gui>Renomear...</gui> no menu de contexto, ou ainda clicar duas vezes sobre a etiqueta na barra lateral.</p>
    
    <p>Para alterar quais as etiquetas que estão associados a uma determinada foto, selecione a foto e use o item de menu <guiseq><gui>Etiquetas</gui><gui>Modificar etiquetas...</gui></guiseq> ou clique com o botão direito do mouse na foto e use o item do menu de contexto <gui>Modificar etiquetas...</gui> e altere a lista separada por vírgulas. Para remover uma etiqueta de uma ou mais fotos, selecione primeiro a etiqueta na barra lateral, em seguida selecione as fotos de que você deseja tirar a etiqueta, e use o item de menu <guiseq><gui>Etiquetas</gui><gui>Remover a etiqueta "[nome]" das fotos</gui></guiseq> ou clique com o botão direito sobre as fotos e use o item <gui>Remover a etiqueta "[nome]" das fotos</gui> do menu de contexto.</p>
    
    <p>Para apagar uma etiqueta completamente, selecione a etiqueta na barra lateral e use o item de menu <guiseq><gui>Etiquetas</gui><gui>Apagar etiqueta "[nome]"</gui></guiseq> ou clique com o botão direito na etiqueta e use o item <gui>Apagar etiqueta "[nome]</gui> do menu de contexto.</p>

    <p>Quando você cria uma etiqueta, ela aparece na barra lateral sob o item <gui>Etiquetas</gui>, que não aparece se não existe pelo menos uma etiqueta. As fotos podem ter várias etiquetas associadas a elas, e quando você clicar sobre o nome de uma etiqueta na barra lateral, você vai ver todas as fotos associadas a essa etiqueta.</p>
    
    <section id="hierarchaicaltags">
        <title>Etiquetas hierárquicas</title>
            <p>O Shotwell oferece suporte a hierarquia entre etiquetas. Você pode reorganizar suas etiquetas arrastando e soltando uma etiqueta sobre outra. Para criar uma nova subetiqueta clique com o botão direito numa etiqueta e use o item <gui>Nova</gui> no menu de contexto.</p>
            
            <p>Etiquetas hierárquica ajudam a organizar sua lista de etiquetas da forma que melhor corresponder à forma como você trabalha ou pensa; por exemplo, você pode armazenar a localização em etiquetas como "Montanha" ou "Praia" dentro de uma etiqueta-pai "Lugares", que podem ser colocados sob a etiqueta "Férias de Verão".</p>
			
			<p>Note que apagar uma etiqueta-pai também vai apagar suas etiquetas-filhas.</p>
    </section>
</page>
