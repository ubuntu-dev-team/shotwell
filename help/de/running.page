<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="running" xml:lang="de">

    <info>
        <link type="guide" xref="index"/>
        <desc>So finden Sie Shotwell im Anwendungsmenü oder lassen Sie es automatisch starten, wenn eine Kamera angeschlossen wird.</desc>
        
        <link type="seealso" xref="other-multiple"/>
        
        <link type="next" xref="formats"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009, 2016-2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2016, 2017</mal:years>
    </mal:credit>
  </info>

	<title>Shotwell ausführen</title>

<p>Sobald Shotwell installiert wurde, ist es in Ihrem <gui>Anwendungs</gui>-Menü verfügbar unter <gui>Grafik</gui> oder <gui>Fotografie</gui>.</p>

<p>Shotwell kann automatisch gestartet werden, sobald eine Kamera an Ihren Rechner angeschlossen wird. Um zu prüfen, ob Ihr System so eingerichtet ist, dass Shotwell gestartet wird, sobald eine Kamera erkannt wird, öffnen Sie <guiseq><gui>Bearbeiten</gui><gui>Einstellungen</gui></guiseq> in der Dateiverwaltung Nautilus und wählen Sie den Reiter <gui>Medien</gui>. Dort finden Sie ein Auswahlfeld namens <gui>Fotos:</gui>, wo Sie Shotwell als die Vorgabe-Anwendung für Fotos festlegen.</p>

<note style="advanced">
 <p>Shotwell kann auch direkt im Erstellungsordner ausgeführt werden. Dies ist jedoch nur zum Testen von Shotwell und für Entwickler empfohlen.</p>
</note>
   
</page>
