<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="edit-adjustments" xml:lang="de">

    <info>
        <link type="guide" xref="index#edit"/>
        <desc>Belichtung, Sättigung, Farbton und Schatten eines Fotos ändern.</desc>
        
        <link type="next" xref="edit-crop"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009, 2016-2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2016, 2017</mal:years>
    </mal:credit>
  </info>

   <title>Farbanpassungen</title>

<p>Der <gui>Anpassen</gui>-Knopf öffnet ein schwebendes Fenster mit einem Histogramm und Schiebereglern zum Verändern der Belichtung, Sättigung, Farbton, Farbtemperatur und Schatten. Das Histogramm verfügt außerdem über Schieberegler zum Anpassen der oberen und unteren Intensitäts-Schwellwerte des Fotos, wodurch der Mittenkontrast erweitert wird.</p>

<p>Der <gui>Verbessern</gui>-Knopf passt das Histogramm und die Schatten-Schieberegler an, um die Qualität eines Fotos zu verbessern.</p>

<p>Wenn die Farben und der Kontrast des Fotos Ihren Vorstellungen entsprechen, klicken Sie auf <gui>OK</gui>, um die Änderungen zu speichern. Durch <gui>Zurücksetzen</gui> wird das Bild in seinen ursprünglichen Zustand zurückversetzt. <gui>Abbrechen</gui> verwirft alle von Ihnen vorgenommenen Änderungen.</p>

<section id="color-adjustments">
 <title>Was bewirken die Farbregler?</title>
 <terms>
  <item>
   <title>Belichtung</title>
   <p>Änderung der Helligkeit, so dass es aussieht, also ob das Foto länger oder kürzer belichtet worden wäre. Verwenden Sie diese Möglichkeit, um über- und unterbelichtete Fotos zu korrigieren.</p>
  </item>
  <item>
   <title>Kontrast</title>
   <p>Ändert den Kontrast eines Fotos. Verwenden Sie dies zur Korrektur von flau aussehenden Fotos oder wenn der Unterschied zwischen hellen und dunklen Bildbereichen zu groß ist.</p>
  </item>
  <item>
   <title>Sättigung</title>
   <p>Dadurch wird bestimmt, wie lebendig Farben wirken. Wenn Ihr Foto grau und ausgewaschen erscheint, sollten Sie die Sättigung erhöhen. Wenn sich Farben zu sehr in den Vordergrund setzen, sollten Sie versuchen, die Sättigung zu verringern.</p>
  </item>
  <item>
   <title>Farbton</title>
   <p>Dies färbt das Foto mit einer Farbe ein. Es ist sinnvoll beim Korrigieren von Fotos mit falschen Weißabgleich-Einstellungen, die typischerweise unnatürliche Farbstiche hervorrufen. Beispielsweise werden im Freien mit der Einstellung »Glühlampenlicht« aufgenommene Fotos einen Blaustich aufweisen.</p>
  </item>
  <item>
   <title>Temperatur</title>
   <p>Dadurch wird bestimmt, wie »warm« oder »kühl« das Bild aussieht. Sie können damit beispielsweise kühle, depressive Szenen lebendiger wirken lassen.</p>
  </item>
  <item>
   <title>Schatten</title>
   <p>Dadurch erscheinen Schattenbereiche heller. Sie können damit Strukturen hervorheben, die in einem Schattenbereich verdeckt sind.</p>
  </item>
  <item>
   <title>Schwellwert der Intensität (Schieberegler im Histogramm)</title>
   <p>Diese Schieberegler bestimmen, wie hell das hellste Weiß und wie dunkel das dunkelste Schwarz dargestellt werden. Sie können damit den Kontrast eines Fotos verändern. Insbesondere ausgewaschen erscheinende Bilder werden von diesen Einstellungen profitieren.</p>
  </item>
 </terms>
</section>
    
</page>
