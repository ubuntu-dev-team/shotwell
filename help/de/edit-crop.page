<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="edit-crop" xml:lang="de">

    <info>
        <link type="guide" xref="index#edit"/>
        <desc>Verbessern Sie die Bildkomposition, indem Sie Teile davon herausschneiden.</desc>
        
        <link type="next" xref="edit-external"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009, 2016-2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2016, 2017</mal:years>
    </mal:credit>
  </info>

   <title>Zuschneiden</title>

    <p>Um den sichtbaren Bereich eines Bildes zu verkleinern und so das Interesse des Betrachters auf einen kleineren Bereich zu lenken, verwenden Sie das Zuschneidewerkzeug. Dieses Werkzeug ist nur im Vollfenster- oder Vollbildmodus verfügbar.</p>

<steps>
 <item>
  <p>Klicken Sie doppelt auf ein Foto, um den Vollfenstermodus zu aktivieren und klicken Sie anschließend auf den <gui>Zuschneiden</gui>-Knopf in der Werkzeugleiste.</p>
 </item>
 <item>
  <p>Ein weißer rechteckiger Zuschnittrahmen wird über das Foto gelegt. Der etwas hellere Bereich des Bildes innerhalb des Zuschnittrahmens repräsentiert das Foto, wie es nach dem Zuschneiden aussehen würde.</p>
 </item>
 <item>
  <p>Wenn Sie den Mauszeiger in der Mitte des Zuschnittrahmens platzieren, können Sie den gesamten Rahmen verschieben. Beim Ziehen der Rahmenecken mit der Maus wird dessen Größe verändert. Während Sie die Größe oder Lage des Zuschnittrahmens verändern, werden darin vier Linien wie in einem Tic-Tac-Toe-Spielfeld angezeigt. Diese entsprechen der <em>Drittelregel</em>.</p>
  <p>Sie können den Zuschnittrahmen auf zahlreiche gängige Größen eingrenzen. Wählen Sie eine Größe aus der Auswahlliste, die Ihren Erfordernissen entspricht. Wenn Sie auf den Umdrehen-Knopf daneben klicken, wechselt die Ausrichtung der ausgewählten Form (von Quer- auf Hochformat).</p>
 </item>
 <item>
  <p>Sobald Sie mit Größe und Bereich des Zuschnittrahmens zufrieden sind, klicken Sie auf den <gui>Zuschneiden</gui>-Knopf. Shotwell zeigt nun das zugeschnittene Bild an.</p>
 </item>
 <item>
  <p>Sollten Sie noch etwas ändern wollen, klicken Sie nochmals auf den <gui>Zuschneiden</gui>-Knopf und passen den Rahmen erneut an.</p>
  <p>Wenn Sie auf <gui>Abbrechen</gui> statt auf <gui>Anwenden</gui> klicken, kehrt Shotwell zu den vorherigen Abmessungen zurück.</p>
 </item>
</steps>

<section id="rule-of-thirds">
 <title>Was ist die Drittelregel?</title>
 <p>Die <em>Drittelregel</em> hilft, ein Foto optisch ansprechend zu gestalten.</p>
 <p>Gehen wir davon aus, dass die Szene durch zwei horizontale und zwei vertikale Linien in ein 3x3-Raster mit gleichen Flächen unterteilt ist. Entsprechend der Drittelregel erhalten Sie sehr wahrscheinlich eine ansprechendere Bildkomposition, wenn Sie bestimmte Anhaltspunkte, wie den Horizont oder den Körper einer Person, an einer dieser Linien ausrichten. Außerdem kann es hilfreich sein, darauf zu achten, wie markante Bildteile von einem Bereich des Rasters in einen anderen übergehen.</p>
 <p>Das Zuschneiden eines Fotos so, dass die Bildobjekte der Drittelregel folgen, führt oft zu einem optisch ansprechenderen Bild.</p>
 <media type="image" src="figures/crop_thirds.jpg">
  <p>Schneiden Sie ein Foto mit den Hilfslinien der »Drittelregel« zu, um die Komposition zu verbessern.</p>
 </media>
</section>
</page>
