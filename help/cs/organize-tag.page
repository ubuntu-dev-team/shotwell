<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="tag" xml:lang="cs">

    <info>
        <link type="guide" xref="index#organize"/>
        <desc>Jak roztřídit fotografie pomocí štítků.</desc>
        
        <link type="seealso" xref="event"/>
        
        <link type="next" xref="title"/>
    </info>

	<title>Přiřazení štítků fotografiím</title>

    <p>Vybraným fotografiím můžete přiřadit jeden nebo více štítků. Štítkem může být jedno nebo i více slov, která přibližují obsah fotografie.</p>
    
    <p>Když chcete fotografii přidat nový štítek, vyberte ji a proveďte něco z následujícího:</p>
    
    <list>
		<item><p>Zvolte <guiseq><gui>Štítky</gui> <gui>Přídat štítky…</gui></guiseq></p></item>
		<item><p>Zmáčkněte <keyseq><key>Ctrl</key><key>T</key></keyseq>.</p></item>
		<item><p>Přetáhněte fotografii na požadovaný štítek v postranním panelu.</p></item>
    </list>
     
    <p>Když použijete <keyseq><key>Ctrl</key> <key>T</key></keyseq> nebo <guiseq><gui>Štítky</gui> <gui>Přidat štítky…</gui></guiseq>, můžete napsat název jednoho nebo i více štítků oddělených čárkami. Jakmile máte štítek jednou vytvořený, můžete jej přejmenovat tak, že jej vyberete v postranním panelu a zvolíte <guiseq><gui>Štítky</gui> <gui>Přejmenovat štítek „[název]“</gui></guiseq> nebo pomocí pravého tlačítka a volbou <gui>Přejmenovat…</gui> nebo dvojitým kliknutím na něj v postranním panelu.</p>
    
    <p>V případě, že chcete změnit přidružení štítku k některé fotografii, vyberte danou fotografii, zvolte <guiseq><gui>Štítky</gui> <gui>Změnit štítky…</gui></guiseq> nebo použijte pravé tlačítko a zvolte <gui>Změnit štítky…</gui> a po té upravte seznam štítků oddělených čárkami. Pokud chcete štítek z jedné nebo více fotografií odstranit, nejprve štítek v postranním panelu vyberte, pak vyberte fotografii, ze které jej chcete odstranit a zvolte <guiseq><gui>Štítky</gui> <gui>Odstranit z fotky štítek „[název]“</gui></guiseq> nebo na ni klikněte pravým tlačítkem a zvolte <gui>Odstranit z fotky štítek „[název]“</gui>.</p>
    
    <p>Jestli chcete smazat štítek kompletně, vyberte jej v postranním panelu a zvolte <guiseq><gui>Štítky</gui> <gui>Smazat štítek „[název]“</gui></guiseq> nebo na něj klikněte pravým tlačítkem o zvolte <gui>Smazat štítek „[název]“</gui>.</p>

    <p>Jakmile vytvoříte štítek, objeví se v postranním panelu pod položkou <gui>Štítky</gui>, která je skrytá v případě, že žádné štítky neexistují. Ke každé fotografii může být přiřazeno i více štítků a když kliknete na název štítku v postranním panelu, uvidíte všechny fotografie, které mají štítek přiřazený.</p>
    
    <section id="hierarchaicaltags">
        <title>Hierarchické štítky</title>
            <p>Shotwell rovněž podporuje hierarchické štítky. Pomocí přetažení štítku na jiný štítek můžete měnit uspořádání. Když chcete vytvořit nový podřízený štítek, klikněte pravým tlačítkem a zvolte <gui>Nový</gui>.</p>
            
            <p>Díky hierarchickým štítkům můžete svůj seznam štítků roztřídit způsobem, který lépe odpovídá vaší práci nebo myšlení. Například může uchovávat štítky jako „hory“ nebo „u moře“ pod společným štítkem „místa“, který sám může být umístěn pod štítkem „dovolené“.</p>
			
			<p>Vezměte na vědomí, že když smažete rodičovský štítek, smažou se i všichni jeho potomci.</p>
    </section>
</page>
