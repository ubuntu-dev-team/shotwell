<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="import-camera" xml:lang="hu">

    <info>
        <link type="guide" xref="index#import"/>
        <desc>Copy photos from a digital camera.</desc>
        
        <link type="next" xref="import-memorycard"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>urbalazs at gmail dot hu</mal:email>
      <mal:years>2015, 2016.</mal:years>
    </mal:credit>
  </info>

	<title>Importing from a camera</title>

    <p>To import photos from a digital camera:</p>

   <steps>
    <item>
     <p>Connect the camera to your computer and switch it on.
    Shotwell will detect it and list it in the sidebar.</p>
    </item>
    <item>
     <p>Select the camera in the sidebar. Previews of each photo on the camera will be displayed.</p>
    </item>
    <item>
     <p>If you like, you can choose a set of specific photos to import. To do this, hold down the <key>Ctrl</key> key and click to select individual photos. You can hold down <key>Shift</key> and click to select a range of photos too.</p>
    </item>
    <item>
     <p>Click either <gui>Import Selected</gui> or <gui>Import All</gui>. The photos will be copied from the camera and saved on your computer.</p>
    </item>
   </steps>
   
    <p>Once the import is complete, you can open the <gui>Last Import</gui> view (in the sidebar) to see all photos that were imported. The Events list (also in the sidebar) will also show the new photos, organized by date.</p>
</page>
